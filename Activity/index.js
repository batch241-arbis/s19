// console.log("Hello World");

let username, password, role;

function provideLogin() {
	updateUsername();
	updatePassword();
	updateRole();
}

function updateUsername() {
	let giveUsername = prompt("Please provide a Username:");
	if (giveUsername == false) {
		alert("Your input must not be empty!");
		updateUsername();
	}
	else {
		username = giveUsername;
	}
}

function updatePassword() {
	let givePassword = prompt("Please provide a Password:");
	if (givePassword == false) {
		alert("Your input must not be empty!");
		updatePassword();
	}
	else {
		password = givePassword;
	}
}

function updateRole() {
	let giveRole = prompt("Please provide a Role:");
	if (giveRole == false) {
		alert("Your input must not be empty!");
		updateRole();
	}
	else {
		switch(giveRole.toLowerCase()){
		case "admin":
			console.log("Welcome back to the class portal, admin!");
			break;
		case "teacher":
			console.log("Welcome back to the class portal, teacher!");
			break;
		case "student":
			console.log("Welcome back to the class portal, student!");
			break;
		default:
			console.log("Role out of range.");
			break;
		}
	}
}

function checkAverage(firstGrade, secondGrade, thirdGrade, fourthGrade) {


	let average = (firstGrade + secondGrade + thirdGrade + fourthGrade) / 4
	average = Math.round(average);

	if (average <= 74) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
	}
	else if (average >= 75 && average <= 79) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
	}
	else if (average >= 80 && average <= 84) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
	}
	else if (average >= 85 && average <= 89) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
	}
	else if (average >= 90 && average <= 95) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
	}
	else if (average >= 96 && average <= 100) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
	}
}

provideLogin();

let firstInput = prompt("Enter your 1st grade:");
let secondInput = prompt("Enter your 2nd grade:");
let thirdInput = prompt("Enter your 3rd grade:");
let fourthInput = prompt("Enter your 4th grade:");

checkAverage(parseInt(firstInput), parseInt(secondInput), parseInt(thirdInput), parseInt(fourthInput));